==============================================================================
界面设计与开发
==============================================================================

.. toctree::
    :maxdepth: 2

    base/index.rst
    common/index.rst
    data/index.rst
    pmwidgets/index.rst
    source/index.rst
    README.md
    base/widgets/README.md
    source/icons/README.md

.. automodule:: pyminer2.ui
    :members:
    :undoc-members: