import json
import requests

__version__ = '1.0.4'


def is_version_latest(app_version: str, remote_version: str):
    app_version_l = app_version.split('.')
    remote_version_l = remote_version.split('.')
    assert len(app_version_l) == len(remote_version_l)
    for velemapp, velemremote in zip(app_version_l, remote_version_l):
        if int(velemapp) < int(velemremote):
            return False
        elif int(velemapp) > int(velemremote):
            print("warning:app version %s is newer than version on server %s." % (app_version, remote_version))
    return True


def query_latest_package_url() -> str:
    url = "http://matopen.com/api/v1/patch/"
    req = requests.get(url).content
    packages = json.loads(req)
    print(packages)
    version = __version__
    latest_index = 0
    for index, package in enumerate(packages):
        if not is_version_latest(version, package['version']):
            version = package['version']
            latest_index = index
    latest_version = version
    url = packages[latest_index]['file_url']
    # print(url)
    return url


def check_update() -> bool:
    """
    如果需要更新，返回True
    反之返回False.
    Returns:

    """
    url = "http://matopen.com/api/v1/patch/"
    req = requests.get(url).content
    packages = json.loads(req)
    print(packages)
    version = __version__
    latest_index = 0
    for index, package in enumerate(packages):
        if not is_version_latest(version, package['version']):
            version = package['version']
            latest_index = index
    latest_version = version
    return not is_version_latest(__version__, latest_version)


if __name__ == '__main__':
    if check_update():
        url = query_latest_package_url()
