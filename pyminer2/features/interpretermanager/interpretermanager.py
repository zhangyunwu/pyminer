"""
settings['interpreters'] =
{'base':{'name':'base',
         'info':'pypy3 python 3.6 compatible',
         'path':'/home/.../python3.8'}
}
"""
import os
from PyQt5.QtWidgets import QTableWidget, QPushButton, QHBoxLayout, QVBoxLayout, QApplication, QWidget

from pyminer2.features.io.settings import Settings
from pmgwidgets import PMGPanelDialog


class Interpreter():
    def __init__(self, absolute_path: str, name: str):
        self.path = ''


class InterpreterManager():
    interpreter_paths = []

    def __init__(self):
        pass


# C:/Users/12957/Documents/Developing/Python/PyMiner_dev_kit/interpreter/python.exe

class InterpreterManagerWidget(QWidget):
    def __init__(self, parent=None):
        super(InterpreterManagerWidget, self).__init__(parent)
        self.table = QTableWidget()
        self.button_add = QPushButton('+')
        self.button_delete = QPushButton('-')
        self.button_edit = QPushButton('Edit')
        self.setLayout(QHBoxLayout())
        self.layout().addWidget(self.table)
        self.button_layout = QVBoxLayout()
        self.layout().addLayout(self.button_layout)
        self.button_layout.addWidget(self.button_add)
        self.button_layout.addWidget(self.button_edit)
        self.button_layout.addWidget(self.button_delete)

        self.button_add.clicked.connect(self.add)
        self.button_edit.clicked.connect(self.edit)
        self.button_delete.clicked.connect(self.delete)

    def gen_info_template(self):
        views = [
            ('line_ctrl', 'name', '名称', ''),
            ('file_ctrl', 'interpreter_path', '解释器路径', '')
        ]

    def add(self):
        views = [
            ('line_ctrl', 'interpreter_name', '名称', ''),
            ('file_ctrl', 'interpreter_path', '解释器路径', '')
        ]

        def set_interpreter_name(settings):
            print('name', settings)
            interpreter_path = settings['interpreter_path']  # dlg.panel.get_ctrl('interpreter_path').get_value()
            dlg.panel.get_ctrl('interpreter_name').set_value(interpreter_name)
            # name = os.path.basename()

        dlg = PMGPanelDialog(parent=self, views=views)
        dlg.panel.set_param_changed_callback('interpreter_path', set_interpreter_name)
        dlg.exec()

    def edit(self):
        pass

    def delete(self):
        pass


if __name__ == '__main__':
    s = Settings()
    from PyQt5.QtWidgets import QApplication

    app = QApplication([])
    w = InterpreterManagerWidget()
    w.show()
    app.exec_()
