import logging
from typing import TYPE_CHECKING, Callable

from qtpy.QtCore import Signal

from pmgwidgets import PMGToolBar, create_icon
from pyminer2.extensions.extensionlib import BaseExtension, BaseInterface

logger = logging.getLogger(__name__)
if TYPE_CHECKING:
    from pyminer2.extensions.extensionlib import extension_lib

from . import preprocess
from .datareplace import DataReplaceForm
from .datamissingvalue import DataMissingValueForm
from .data_filter import DataFilterForm


logger = logging.getLogger(__name__)
if TYPE_CHECKING:
    from pyminer2.extensions.extensionlib import extension_lib


class PMDrawingsToolBar(PMGToolBar):
    drawing_item_double_clicked_signal: 'Signal' = Signal(str)
    extension_lib: 'extension_lib' = None
    variable = None

    def __init__(self):
        super().__init__()
        self.current_var_name = ''
        self.current_dataset_dtype = set()  # 保存当前数据集中存在的数据类型，确保不会重复
        self.add_tool_button('button_data_filter', '筛选',
                             create_icon(':/color/source/theme/color/icons/filter.svg'))
        self.add_tool_button('button_data_replace', '查找替换',
                             create_icon(':/color/source/theme/color/icons/replace.svg'))
        self.add_tool_button('button_data_info', '数据信息',
                             create_icon(':/color/source/theme/color/icons/data_info.svg'))
        self.add_tool_button('button_data_column', '列名处理',
                             create_icon(':/color/source/theme/color/icons/column.svg'))
        self.addSeparator()
        self.add_tool_button('button_data_role', '数据角色',
                             create_icon(':/color/source/theme/color/icons/data_role.svg'))
        self.add_tool_button('button_data_partition', '数据分区',
                             create_icon(':/color/source/theme/color/icons/data_partition.svg'))
        self.addSeparator()
        self.add_tool_button('button_data_add_row', '新增行',
                             create_icon(':/color/source/theme/color/icons/add_row.svg'))
        self.add_tool_button('button_data_add_column', '新增列',
                             create_icon(':/color/source/theme/color/icons/add_col.svg'))
        self.add_tool_button('button_data_delete_row', '删除行',
                             create_icon(':/color/source/theme/color/icons/delete_row.svg'))
        self.add_tool_button('button_data_delete_column', '删除列',
                             create_icon(':/color/source/theme/color/icons/delete_col.svg'))
        self.addSeparator()
        self.add_tool_button('button_data_missing', '缺失值',
                             create_icon(':/color/source/theme/color/icons/missing_value.svg'))
        self.add_tool_button('button_data_sample', '抽样',
                             create_icon(':/color/source/theme/color/icons/sample.svg'))
        self.add_tool_button('button_data_transposition', '转置',
                             create_icon(':/color/source/theme/color/icons/transposition.svg'))
        self.addSeparator()
        self.add_tool_button('button_data_merge_v', '纵向合并',
                             create_icon(':/color/source/theme/color/icons/merge_v.svg'))
        self.add_tool_button('button_data_merge_h', '横向合并',
                             create_icon(':/color/source/theme/color/icons/merge_h.svg'))
        self.addSeparator()
        self.add_tool_button('button_data_join', '连接',
                             create_icon(':/color/source/theme/color/icons/data_join.svg'))
        self.add_tool_button('button_data_scale', '标准化数据',
                             create_icon(':/color/source/theme/color/icons/scale.svg'))

        self.addSeparator()

    def on_data_selected(self, data_name: str):
        """
        当变量树中的数据被单击选中时，调用这个方法。
        """
        self.current_var_name = data_name
        self.variable = self.extension_lib.Data.get_var(data_name)
        logger.info('Variable clicked. Name is \'' + data_name)

    def on_data_modified(self, var_name: str, variable: object, data_source: str):
        """
        在数据被修改时，调用这个方法。
        """
        pass

    def on_close(self):
        self.hide()
        self.deleteLater()

    def bind_events(self):
        """
        绑定事件。这个将在界面加载完成之后被调用。
        """
        self.get_control_widget('button_data_filter').clicked.connect(self.show_data_filter)
        self.get_control_widget('button_data_replace').clicked.connect(self.show_data_replace)
        self.get_control_widget('button_data_missing').clicked.connect(self.show_data_missing)
        # self.get_control_widget('button_data_info').clicked.connect(self.show_data_info)
        # self.get_control_widget('button_data_column').clicked.connect(self.show_data_column)
        # self.get_control_widget('button_data_role').clicked.connect(self.show_data_role)
        # self.get_control_widget('button_data_partition').clicked.connect(self.show_data_partition)
        # self.get_control_widget('button_data_add_row').clicked.connect(self.show_data_add_row)
        # self.get_control_widget('button_data_add_column').clicked.connect(self.show_data_add_column)
        # self.get_control_widget('button_data_delete_row').clicked.connect(self.show_data_delete_row)
        # self.get_control_widget('button_data_delete_column').clicked.connect(self.show_data_delete_column)
        # self.get_control_widget('button_data_partition').clicked.connect(self.show_data_info)
        # self.get_control_widget('button_data_partition').clicked.connect(self.show_data_info)
        # self.get_control_widget('button_data_partition').clicked.connect(self.show_data_info)
        # self.get_control_widget('button_data_partition').clicked.connect(self.show_data_info)
        # self.get_control_widget('button_data_partition').clicked.connect(self.show_data_info)
        self.get_control_widget('button_data_scale').clicked.connect(self.show_data_scale)

        self.extension_lib.Signal.get_close_signal().connect(self.on_close)

    def show_data_missing(self):
        self.data_missing = DataMissingValueForm()
        self.data_missing.show()

    def show_data_scale(self):
        self.data_scale = DataFilterForm()
        self.data_scale.show()

    def show_data_filter(self):
        """
        显示 "数据筛选" 窗口
        """
        self.data_filter = DataFilterForm()
        if len(self.current_var_name) > 0:
            self.data_filter.current_dataset = self.variable
            self.data_filter.current_dataset_name = self.current_var_name
            self.data_filter.comboBox_columns.addItems(list(self.variable.columns))
            for col in self.variable.columns:
                self.current_dataset_dtype.add(str(self.variable.loc[:, col].dtype))
            self.data_filter.comboBox_dtype.addItems(list(self.current_dataset_dtype))
            self.data_filter.dataset_init()
        else:
            self.data_filter.setWindowTitle(self.data_filter.windowTitle() + '--未选择数据')

        self.data_filter.signal_data_change.connect(self.slot_var_reload)  # 信号处理

        self.data_filter.show()

    def slot_var_reload(self, str, dict):
        """
        刷新工作区间中的变量
        """
        import pandas as pd
        self.extension_lib.Data.set_var(str, pd.DataFrame.from_dict(dict))

    def show_data_replace(self):
        self.data_replace = DataReplaceForm()
        self.data_replace.show()
    #
    # def show_data_info(self):
    #     self.data_info=preprocess.DataInfoForm()
    #     self.data_info.show()
    #
    # def show_data_column(self):
    #     self.data_column=preprocess.DataColumnDescForm()
    #     self.data_column.show()
    #
    # def show_data_role(self):
    #     self.data_role=preprocess.DataRoleForm()
    #     self.data_role.show()
    #
    # def show_data_partition(self):
    #     self.data_partition=preprocess.DataPartitionForm()
    #     self.data_partition.show()
    #
    # def show_data_add_row(self):
    #     self.data_add_row=preprocess.DataNewColumnForm()
    #     self.data_add_row.show()
    #
    #
    #
    # def show_data_add_column(self):
    #     self.data_add_column=preprocess.DataNewColumnForm()
    #     self.data_add_column.show()
    #
    # def show_data_delete_row(self):
    #     self.data_delete_row=preprocess.DataDeleteRowForm()
    #     self.data_delete_row.show()
    #
    # def show_data_delete_column(self):
    #     self.data_delete_column=preprocess.DataDeleteColumnForm()
    #     self.data_delete_column.show()


class Extension(BaseExtension):
    if TYPE_CHECKING:
        interface: 'DrawingsInterface' = None
        widget: 'PMDrawingsToolBar' = None
        extension_lib: 'extension_lib' = None

    def on_loading(self):
        global _
        _ = self.extension_lib.Program._
        self.extension_lib.Program.add_translation('zh_CN', {'Drawings': '绘图', 'Variable Selected': '选择的变量',
                                                             'No Variable': '尚未选择变量'})

    def on_load(self):
        drawings_toolbar: 'PMDrawingsToolBar' = self.widgets['PMDrawingsToolBar']
        drawings_toolbar.extension_lib = self.extension_lib
        self.drawings_toolbar = drawings_toolbar
        self.interface.drawing_item_double_clicked_signal = drawings_toolbar.drawing_item_double_clicked_signal

        self.interface.drawing_item_double_clicked_signal.connect(self.interface.on_clicked)
        self.interface.drawings_toolbar = drawings_toolbar

        self.extension_lib.Data.add_data_changed_callback(drawings_toolbar.on_data_modified)
        self.extension_lib.Signal.get_widgets_ready_signal().connect(self.bind_events)

    def bind_events(self):
        workspace_interface = self.extension_lib.get_interface('workspace_inspector')
        workspace_interface.add_select_data_callback(self.drawings_toolbar.on_data_selected)


class PreprocessInterface(BaseInterface):
    drawing_item_double_clicked_signal: 'Signal' = None
    drawings_toolbar: 'PMDrawingsToolBar' = None

    def on_clicked(self, name: str):
        pass
        # print('interface', name)

    def add_graph_button(self, name: str, text: str, icon_path: str, callback: Callable, hint: str = ''):
        """
        添加一个绘图按钮。name表示按钮的名称,text表示按钮的文字，icon_path表示按钮的图标路径，callback表示按钮的回调函数
        hint表示的就是按钮鼠标悬浮时候的提示文字。
        例如：
        extension_lib.get_interface('drawings_toolbar').add_graph_button('aaaaaa','hahahaahahah',
                                                                         ':/pyqt/source/images/lc_searchdialog.png',lambda :print('123123123'))
        """
        self.drawings_toolbar.add_toolbox_widget(name, text, icon_path, hint, refresh=True)
