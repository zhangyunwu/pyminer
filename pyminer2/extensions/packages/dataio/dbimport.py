import sample
class dbimportutils():
    def import_mysql_display(self):
        """
        显示"从mysql数据库导入表"窗口
        """

        self.import_mysql_database = sample.ImportMysql()
        self.import_mysql_database.exec()

    def import_oracle_display(self):
        """
        显示"从Oracle数据库导入表"窗口
        """
        self.import_oracle_database = sample.ImportOracle()
        self.import_oracle_database.exec()

    def import_postgresql_display(self):
        """
        显示"从PostgreSQL数据库导入表"窗口
        """
        self.import_postgresql_database = sample.ImportPostgreSQL()
        self.import_postgresql_database.exec()

    def doImportEngine(self):
        return {
            "mysql": self.import_mysql_display,
            "oracle": self.import_oracle_display,
            "postgresql": self.import_postgresql_display
        }
