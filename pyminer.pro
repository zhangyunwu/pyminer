SOURCES         = app2.py \
                  pyminer2/features/base.py

FORMS           = pyminer2/ui/base/aboutMe.ui \
                  pyminer2/ui/base/first_form.ui

TRANSLATIONS    = languages/en/en.ts \
                  languages/zh_CN/zh_CN.ts \
                  languages/zh_TW/zh_TW.ts

CODECFORTR      = UTF-8
CODECFORSRC     = UTF-8

# pylupdate5.exe pyminer.pro
# linguist.exe languages\en\en.ts languages\zh_CN\zh_CN.ts languages\zh_TW\zh_TW.ts