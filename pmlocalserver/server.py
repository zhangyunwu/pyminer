from flask import Flask

from flask_cors import CORS
import threading

server = Flask(__name__)
CORS(server, supports_credentials=True)  # 解决跨域

server_thread = threading.Thread(target=server.run)  # 这个线程与主界面没有任何交互，直接用系统内建的线程库即可，保证其安全性。
server_thread.setDaemon(True)


if __name__ == '__main__':
    server_thread.start()
